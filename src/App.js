import React from 'react';
import './App.css';
import FCXViewer from './modules/cesium';

function App() {
  return (
    <div className="App">   
        <FCXViewer/>
    </div>
  );
}

export default App;
